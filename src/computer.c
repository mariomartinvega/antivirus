/**
 * This file defines a computer.
 *
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "greenfoot.h"

/**
 * Initialize its image.
 */
void startComputer(Actor computer) {
	setImageFile(computer, "computer.png");
}

/**
 * Act - do whatever the computer wants to do. This function
 * is called whenever the 'Act' or 'Run' button gets pressed
 * in the environment.
 */
void actComputer(Actor computer) {

}

