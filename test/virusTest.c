/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "jni_test.h"
#include "virus.c"

World internet;
Actor virus;

void setUp() {
	// Given a lymph fluid with its virus
	internet = newWorld("internet");
	int actorsNumber;
	Actor* actors = getActors(internet, "virus", &actorsNumber);
	assertEquals_int(3, actorsNumber);
	virus = actors[0];
}

void testMove() {
	// Given the initial position of the first virus
	setUp();
	int x = getX(virus);
	int y = getY(virus);

	// When the random number is less than or equal to 90
	setRandom(90);
	actVirus(virus);

	// Then the virus does not turn and moves 5
	assertEquals_int(x+5, getX(virus));
	assertEquals_int(  y, getY(virus));
	assertEquals_int(  0, getRotation(virus));
}


void testMoveAtEdge() {
	// Given the edge position of the first virus
	setUp();
	int x = getWidth(internet)-1;
	int y = 100;
	setLocation(virus, x, y);


	// When the random number is less than or equal to 90
	setRandom(90);
	actVirus(virus);

	// Then the virus moves 1 and turns 17
	assertEquals_int(  x, getX(virus));
	assertEquals_int(y+1, getY(virus));
	assertEquals_int( 17, getRotation(virus));
}


void testTwoMoveAtEdge() {
	// Given the edge position of the first virus
	setUp();
	int x = getWidth(internet)-1;
	int y = 100;
	setLocation(virus, x, y);


	// When the random number is less than or equal to 90
	setRandom(90);
	actVirus(virus);
	setRandom(90);
	actVirus(virus);

	// Then the virus moves 4 and turns 17+17
	assertEquals_int(  x, getX(virus));
	assertEquals_int(y+4, getY(virus));
	assertEquals_int( 34, getRotation(virus));
}


void testTurnRight() {
	// Given the initial position of the first virus
	setUp();
	int x = getX(virus);
	int y = getY(virus);

	// When the random number is greater than 90
	setRandom(91); // random number is greater than 90
	setRandom(90); // random number to turn 45 = 90 - 45
	actVirus(virus);

	// Then the virus turns 45 degrees and moves diagonally
	assertEquals_int(x+4, getX(virus));
	assertEquals_int(y+4, getY(virus));
	assertEquals_int( 45, getRotation(virus));
}


void testTurnLeft() {
	// Given the initial position of the first virus
	setUp();
	int x = getX(virus);
	int y = getY(virus);

	// When the random number is greater than 90
	setRandom(91); // random number is greater than 90
	setRandom(0);  // random number to turn -45 = 0 - 45
	actVirus(virus);

	// Then the virus turns -45 degrees and moves diagonally
	assertEquals_int(x+4, getX(virus));
	assertEquals_int(y-4, getY(virus));
	assertEquals_int(315, getRotation(virus));
}


void testTurnRightAtEdge() {
	// Given the edge position of the first virus
	setUp();
	int x = getWidth(internet)-1;
	int y = 150;
	setLocation(virus, x, y);


	// When the random number is greater than 90
	setRandom(91); // random number is greater than 90
	setRandom(90); // random number to turn 45 = 90 - 45
	actVirus(virus);

	// Then the virus moves 4 turns 62
	assertEquals_int(    x, getX(virus));
	assertEquals_int(  y+4, getY(virus));
	assertEquals_int(45+17, getRotation(virus));
}


void testTurnLeftAtEdge() {
	// Given the edge position of the first virus
	setUp();
	int x = getWidth(internet)-1;
	int y = 150;
	setLocation(virus, x, y);


	// When the random number is greater than 90
	setRandom(91); // random number is greater than 90
	setRandom(0);  // random number to turn -45 = 0 - 45
	actVirus(virus);

	// Then the virus moves -2 and turns -28
	assertEquals_int(        x, getX(virus));
	assertEquals_int(      y-2, getY(virus));
	assertEquals_int(360+17-45, getRotation(virus));
}


void testTurnRight90() {
	// Given a virus that has rotated 90 to the right
	setUp();
	for (int k=0; k<2;k++){
		setRandom(91); // random number to turn
		setRandom(90); // random number to turn 45 = 90 - 45
		actVirus(virus);
	}
	int x = getX(virus);
	int y = getY(virus);

	// When the virus does not turn again
	setRandom(0);
	actVirus(virus);

	// Then the virus goes down 5
	assertEquals_int(  x, getX(virus));
	assertEquals_int(y+5, getY(virus));
}


void testTurnRight180() {
	// Given a virus that has rotated 180 degrees to the right
	setUp();
	for (int k=0; k<4;k++){
		setRandom(91); // random number to turn
		setRandom(90); // random number to turn 45 = 90 - 45
		actVirus(virus);
	}
	int x = getX(virus);
	int y = getY(virus);

	// When the virus does not turn again
	setRandom(0);
	actVirus(virus);

	// Then the virus goes back 5
	assertEquals_int(x-5, getX(virus));
	assertEquals_int(  y, getY(virus));
}


void testTurnRight270() {
	// Given a virus that has rotated 270 degrees to the right
	setUp();
	for (int k=0; k<6;k++){
		setRandom(91); // random number to turn
		setRandom(90); // random number to turn 45 = 90 - 45
		actVirus(virus);
	}
	int x = getX(virus);
	int y = getY(virus);

	// When the virus does not turn again
	setRandom(0);
	actVirus(virus);

	// Then the virus goes up 5
	assertEquals_int(  x, getX(virus));
	assertEquals_int(y-5, getY(virus));
}


void testTurnLeft90() {
	// Given a virus that has rotated 90 to the left
	setUp();
	for (int k=0; k<2;k++){
		setRandom(91); // random number to turn
		setRandom(0);  // random number to turn -45 = 0 - 45
		actVirus(virus);
	}
	int x = getX(virus);
	int y = getY(virus);

	// When the virus does not turn again
	setRandom(0);
	actVirus(virus);

	// Then the virus goes up 5
	assertEquals_int(  x, getX(virus));
	assertEquals_int(y-5, getY(virus));
}


void testTurnLeft180() {
	// Given a virus that has rotated 180 degrees to the left
	setUp();
	for (int k=0; k<4;k++){
		setRandom(91); // random number to turn
		setRandom(0);  // random number to turn -45 = 0 - 45
		actVirus(virus);
	}
	int x = getX(virus);
	int y = getY(virus);

	// When the virus does not turn again
	setRandom(0);
	actVirus(virus);

	// Then the virus goes back 5
	assertEquals_int(x-5, getX(virus));
	assertEquals_int(  y, getY(virus));
}


void testTurnLeft270() {
	// Given a virus that has rotated 270 degrees to the left
	setUp();
	for (int k=0; k<6;k++){
		setRandom(91); // random number to turn
		setRandom(0);  // random number to turn -45 = 0 - 45
		actVirus(virus);
	}
	int x = getX(virus);
	int y = getY(virus);

	// When the virus does not turn again
	setRandom(0);
	actVirus(virus);

	// Then the virus goes down 5
	assertEquals_int(  x, getX(virus));
	assertEquals_int(y+5, getY(virus));
}


void testLookForComputer() {
	// Given a computer in the same position as the virus
	setUp();
	Actor computer = newActor("computer");
	int x = getX(virus);
	int y = getY(virus);
	addActorToWorld(internet, computer, x, y);
	assertEquals_int(1, getActorsNumberAt(internet, x, y, "computer"));

	// When the virus checks that there is a computer
	actVirus(virus);

	// Then you can listen to how the virus eats the computer
	assertEquals_String("slurp.wav", getPlayedSound());
	assertEquals_int(0, getActorsNumberAt(internet, x, y, "computer"));
}

void testLookForAllComputer() {
	// Given a virus that has eaten computers and there are 3 left
	setUp();
	int computersNumber;
	Actor* computer = getActors(internet, "computer", &computersNumber);
	for (int k=0; k<computersNumber-3; k++) {
		setLocation(virus, getX(computer[k]), getY(computer[k]));
		actVirus(virus);
	}

	// When the virus eats the next computer
	setLocation(virus, getX(computer[7]), getY(computer[7]));
	actVirus(virus);

	// Then the trumpets are heard and the game over
	assertTrue(isPlayed("hooray.wav"));
	assertTrue(isWorldStopped());
}

void testLookForAllComputerBis() {
	// Given a virus that has eaten computers and there are 3 left
	setUp();
	int computersNumber;
	Actor* computer = getActors(internet, "computer", &computersNumber);
	for (int k=0; k<computersNumber-3; k++) {
		setLocation(virus, getX(computer[k]), getY(computer[k]));
		actVirus(virus);
		assertEquals_String("slurp.wav", getPlayedSound());
		// To discard the sound played
	}

	// When the virus eats the next computer
	int x = getX(computer[7]);
	int y = getY(computer[7]);
	setLocation(virus, x, y);
	actVirus(virus);
	assertEquals_int(0, getActorsNumberAt(internet, x, y, "computer"));
	assertEquals_String("slurp.wav", getPlayedSound()); // To discard the
	                                                    // sound played

	// Then the trumpets are heard and the game over
	assertEquals_String("hooray.wav", getPlayedSound());
	assertTrue(isWorldStopped());
}

void testLookForAllComputerBisBis() {
	// Given a virus that has eaten computers and there are 3 left
	setUp();
	int computersNumber;
	Actor* computer = getActors(internet, "computer", &computersNumber);
	for (int k=0; k<computersNumber-3; k++) {
		setLocation(virus, getX(computer[k]), getY(computer[k]));
		actVirus(virus);
	}

	// When the virus eats the next computer
	int x = getX(computer[7]);
	int y = getY(computer[7]);
	setLocation(virus, x, y);
	runOnceActors(&virus,1); // To discard the sounds played
				                 // before eating the eighth computer
	assertEquals_int(0, getActorsNumberAt(internet, x, y, "computer"));
	assertEquals_String("slurp.wav", getPlayedSound()); // To discard the
	                                                    // sound played

	// Then the trumpets are heard and the game over
	assertEquals_String("hooray.wav", getPlayedSound());
	assertTrue(isWorldStopped());
}
