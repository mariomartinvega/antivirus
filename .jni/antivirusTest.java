/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

import org.junit.Test;
import org.junit.runner.RunWith;
import greenfoot.junitUtils.runner.GreenfootRunner;
@RunWith(GreenfootRunner.class)
public class antivirusTest {
    @Test
    public native void testChangeImage();

    @Test
    public native void testChangeImageAgain();

    @Test
    public native void testChangeImageAgainAgain();

    @Test
    public native void testChangeImageAgainAgainAgain();

    @Test
    public native void testGetImage();

    @Test
    public native void testGetRotation();

    @Test
    public native void testGetX();

    @Test
    public native void testGetY();

    @Test
    public native void testLookForVirus();

    @Test
    public native void testLookForVirusBis();

    @Test
    public native void testMove();

    @Test
    public native void testMoveBis();

    @Test
    public native void testMoveBisBis();

    @Test
    public native void testTurnLeft();

    @Test
    public native void testTurnLeft180();

    @Test
    public native void testTurnLeft270();

    @Test
    public native void testTurnLeft90();

    @Test
    public native void testTurnLeftBis();

    @Test
    public native void testTurnLeftBisBis();

    @Test
    public native void testTurnRight();

    @Test
    public native void testTurnRight180();

    @Test
    public native void testTurnRight270();

    @Test
    public native void testTurnRight90();

    @Test
    public native void testTurnRightBis();

    @Test
    public native void testTurnRightBisBis();

    static {
        System.load(new java.io.File(".jni", "antivirusTest_jni.so").getAbsolutePath());
    }
}
