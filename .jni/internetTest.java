/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

import org.junit.Test;
import org.junit.runner.RunWith;
import greenfoot.junitUtils.runner.GreenfootRunner;
@RunWith(GreenfootRunner.class)
public class internetTest {
    @Test
    public native void testStartGame();

    static {
        System.load(new java.io.File(".jni", "internetTest_jni.so").getAbsolutePath());
    }
}
